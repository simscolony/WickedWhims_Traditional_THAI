
# แปลฟรีและสามารถดาวโหลดฟรี ไม่มีการครอบลิงค์หาเงิน หาพบเห็นให้แจ้งแอดมินของเว็บนั้นๆ
## ซื้อเกมได้ที่ [BUT SIMS4](https://www.cdkeys.com/pc/games/the-sims-4-standard-edition-pc-cd-key-origin?mw_aref=simscolony)

กรุณาเลือกติดตั้งระหว่าง WickedWhims และ WonderfulWhims อย่างใดอย่างหนึ่งเท่านั้นไม่สามารถติดตั้งพร้อมกันได้
กรุณาเลือกลงเพียงแบบเดียวเท่านั้น
![CLUB-ACIYORUZ-💓Sims-4-Wicked-Whims-Turkce-inceleme](https://user-images.githubusercontent.com/13219372/127035913-4855b0af-ebc4-4239-9b25-57c0f2bb4267.jpg)


# แปลไทย WickedWhims อัพเดท 17 มีนาคม 2565 - อัตราการแปล 100%
[loverslab แปลไทย WickedWhims](https://www.loverslab.com/files/file/5755-sims-4-wickedwhims-thai-support-wickedwhims-v167c-18-december-2021/)



| SIMSCOLONY THAI| WickedWhims 17-03-2022|รายละเอียด|
| ------------- | ------------- | ------------- |
| WickedWhims V47| [แปลไทย WickedWhims 46](https://modsfire.com/5mXnKq3M7b143pR) |แปลไทยอย่างเดียว|
| WickedWhims V47 สำหรับ MAC และ Windows|  [WickedWhims แปลไทย พร้อมตัวเล่น](https://modsfire.com/59eY0BjZOl9nab2) |แปลไทย พร้อมตัวเล่น สำหรับ MAC และ Windows สำหรับเครื่องที่ลงลิงค์ 1-2 แล้วไม่ขึ้น ไม่ต้องโหลด Wicked ภาษาอังกฤษมาลง - ลงตัวเดียวพร้อมเล่นมีตัวเกมและแปลไทยแล้ว|



![LOGO_WickedWhims](https://img.itch.zone/aW1nLzMzMDExODAucG5n/original/mSNqg3.png)
# Update Last Support 
## Public Version WickedWhims v167g - MARCH 15TH 2022
##  patreon Version WickedWhim﻿s  v170.3 - MARCH 15TH 2022

### HOW TO Downlodws MOD PLAY


# วิธีการลง
1. ติดตั้ง MOD ภาพไทย
https://simscolony.github.io/TS4THDEMO/

2. ดาวโหลด MOD หลัก WickedWhims  รุ่นฟรีดาวโหลดทาง
https://turbodriver.itch.io/wickedwhims
WickedWhims Mod made by TURBODRIVER   [Download wickedwhims](https://wickedwhimsmod.com/download/) OR
[loverslab](https://www.loverslab.com/files/file/5755-sims-4-thai-translation-for-wickedwhims-435140c-16-april-2019/)


ดาวโหลด MOD หลัก WickedWhims  รุ่นสนับสนุับ
Patreon https://www.patreon.com/wickedwoohoo


3. นำลงใส่ใน FLoder Mods

![wick](https://user-images.githubusercontent.com/13219372/127035833-41096a39-6cce-4852-8207-d3f88aae143a.jpg)


## แปลไทย WonderfulWhims

![LOGO_WonderfulWhims](https://img.itch.zone/aW1nLzQyNjc4NDEucG5n/original/bivTAu.png)
https://simscolony.github.io/WonderfulWhims_Traditional_THAI/


# หากพบปัญหา แจ้งงานแปล
## ติดต่อสอบถามได้ที่ [SIMSCOLONY](https://www.facebook.com/SimsColony/)

# ภายใต้การอนุญาติ 

Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-nc-sa/4.0/

https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

